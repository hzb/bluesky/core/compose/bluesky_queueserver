#! /bin/sh


# Force the reinstall of the beamlinetools so that we take the mounted version
python3 -m pip install -e /opt/bluesky/beamlinetools
python3 -m pip install bluesky_queueserver_api

/usr/local/bin/start-re-manager --startup-dir /opt/bluesky/ipython/profile_root/startup --user-group-permissions=/opt/bluesky/beamlinetools/beamlinetools/beamline_config/user_group_permissions.yaml --keep-re --redis-addr localhost:6379 --zmq-publish-console ON --use-ipython-kernel ON --redis-name-prefix secondary