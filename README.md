## Bluesky Queueserver

This repo contains a compose file which:

1. Starts a redis server (data is persisted in the host under the dir `./redis_data`)
2. Starts a zmq proxy to buffer documents coming from the RE and allow clients to subscribe (e.g. plotting)
3. Starts a queueserver using the startup file defined in startup.py. 

The file `start_re.sh` expects the package structure defined in `vanilla_beamlinetools` to know where to start the ipython kernel from. 

### Required Configuration

You may need to change the Tiled API Key and URL. Normally these are defined by environment variables set in the bluesky deployment. 

### How to Start

start with:

`docker-compose up -d`


### Ports and Networking

The containers run with host networking (because it was easier to set up and we need to connect to external services like Tiled). In the current configuration the following host ports are used:

| Port   |      Use      |  
|----------|-------------|
| 6379 | redis | 
| 60615 |   queueserver control   |  
| 60625| queueserver console  | 
| 5577| 0MQ Port subscribed to RE| 
| 5578| 0MQ Port for clients to subscribe to| 


